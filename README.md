# Tablinator
by BERTHOLON Noah

visit [my site](http://noahbertholon.fr/projets) to see other projects

------

# Description
Debugging with terminal can be tedious, even more if your self-made tables aren't well align.\
This project fix this issue with a simple header to include.

# Installation
1. Clone the repository \
`git clone https://gitlab.com/HanoBaha/tablinator.git`
2. Copy the header where you need it
3. Enjoy your tables.

# Usage
## Creating a table
You need to specify row and column count to the template, and a name for your table as constructor's parameter.
```cpp
Table<10, 5> T("My awesome table"); // 10 rows of 5 columns
```

## Setup column
Use `define_column` method to setup individual column.\
You need to specify the column index, its name and its width.\
Content overflowing will be automatically croped.
```cpp
T.define_column(0, "Waw", 7); // The first column will be named "Waw" and will have a width of 7
```

## Insert element
Use operator `[row][column]` to access a specific cell.\
Use operator `=` to assign its value.
```cpp
T[5][3] = 53;
```
If you want to insert your own data structure, make sure to write a method that return it as a char*, int or float.

## Draw table
Use `print` method to draw your table.
```cpp
T.print();
```