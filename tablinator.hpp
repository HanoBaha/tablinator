#include <iostream>
#include <assert.h>
#include <ostream>
#include <cstring>

class Cell
{
public:
    Cell() : width(0) {}
    ~Cell() { delete [] content; }

    void set_width(std::size_t width_) { 
        width = width_; 
        content = new char[width + 1]; 
        content[width] = '\0';
    }

    char* c_str() { return content; }

    Cell& operator=(const char* str) 
    {
        strncpy(content, str, width); 
        return *this;
    }

    Cell& operator=(int i)
    {
        *this =  ((i < 0 ? "" : " ") + std::to_string(i)).c_str();
        return *this;
    }

    Cell& operator=(float f)
    {
        *this =  ((f < 0 ? "" : " ") + std::to_string(f)).c_str();
        return *this;
    }

private:
    std::size_t width;
    char* content;
};



class Row
{
public:
    Row() {}
    ~Row() {
        delete [] m_cells_array;
    }
    
    void define_row(int nb_column, int* width_array) {
        m_column_count = nb_column;
        m_cells_array = new Cell[m_column_count];
        m_width_array = width_array;
    }

    Cell& operator[](std::size_t index) { assert(index < m_column_count); return m_cells_array[index]; }

    void set_width(std::size_t index_column, std::size_t width) { 
        assert(index_column < m_column_count); 
        m_cells_array[index_column].set_width(width); 
    }

    void print()
    {
        int accumulator = 0;
        printf("│");
        for(int i = 0; i < m_column_count; i++)
        {
            accumulator += m_width_array[i] + 1;
            printf(" %s\x1b[1000D\x1b[%iC│", 
                m_cells_array[i].c_str(), 
                accumulator
            );
        }
        printf("\n");
    }

private:
    int m_column_count;
    Cell* m_cells_array;
    int* m_width_array;

};



#define MIN(x, y) (x < y ? x : y)


class Table
{
public:
    Table(int nb_row, int nb_column, const char* name) : 
        m_rows_count(nb_row), m_column_count(nb_column),
        m_rows_array(new Row[m_rows_count + 1]), // Row[0] store headers
        m_width_array(new int[m_column_count]),
        m_title_length(strlen(name)),
        m_title(new char[m_title_length + 1])    // We have to store one more char for \0
    {
        for(int i = 0; i < m_rows_count + 1; i++)
            m_rows_array[i].define_row(m_column_count, m_width_array);

        strncpy(m_title, name, m_title_length + 1);
    }

    ~Table() {
        delete [] m_rows_array;
        delete [] m_width_array;
        delete [] m_title;
    };

    /**
     * @brief Setup a column to the specified format
     * 
     * @param index_column Index of the formated column
     * @param name Name of the formated column
     * @param width Width (as terminal character) of the formated column
     */
    void define_column(int index_column, const char* name, int width)
    {
        assert(index_column < m_column_count);
        for(int i = 0; i < m_rows_count + 1; i++)
            m_rows_array[i].set_width(index_column, width);
        
        m_width_array[index_column] = width + 2; // space arround
        
        m_rows_array[0][index_column] = name;
    }

    /**
     * @brief Call when table is fully defined
     * 
     */
    void finalize_table()
    {
        if(m_title_length > total_width() - 4) {
            m_title_length = total_width() - 4;

            char* tmp = m_title;
            m_title = new char[m_title_length + 1];
            strncpy(m_title, tmp, m_title_length);
            m_title[m_title_length] = '\0';

            delete [] tmp;
        }
    }

    Row& operator[](std::size_t index) { assert(index < m_rows_count); return m_rows_array[index + 1]; }

    void print()
    {
        int arroud_title = (total_width() - 2 - utf8len(m_title)) / 2;
        
        
        // Upper bar
        print_bar("┌", "─", "┐");
        
        // Title
        printf("│\x1b[%iC%s\x1b[%iC│\n", 
            arroud_title, 
            m_title, 
            int(total_width() - 2 - arroud_title - utf8len(m_title))
        );

        // Under title separator
        print_bar("├", "┬", "┤");

        // Header line
        m_rows_array[0].print();

        // Under header separator
        print_bar("├", "┼", "┤");

        // Print all lines
        for(int i = 1; i < m_rows_count + 1; i++) m_rows_array[i].print();

        // Bottom bar
        print_bar("└", "┴", "┘");
    }

private:
    int m_rows_count, m_column_count;


    // rows_array[0] is header
    Row* m_rows_array;
    int* m_width_array;
    int m_title_length;
    char* m_title;

    // returns the number of utf8 code points in the buffer at s
    size_t utf8len(const char *s)
    {
        size_t len = 0;
        for (; *s; ++s) if ((*s & 0xC0) != 0x80) ++len;
        return len;
    }

    int total_width()
    {
        int res = 0;
        for(int i = 0; i < m_column_count; i++)
            res += m_width_array[i];
        return res + m_column_count + 1; // count separators
    }

    void print_bar(const char* left, const char* mid, const char* right)
    {
        printf("%s", left);
        for(int i = 0; i < m_column_count; i++) {
            for(int j = 0; j < m_width_array[i]; j++)
                printf("─");
            printf("%s", mid);
        }
        printf("\x1b[1D%s\n", right);
    }
};

#undef MIN